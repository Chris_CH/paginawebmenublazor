﻿using Android.App;
using Android.OS;
using Android.Runtime;
using AndroidX.AppCompat.App;
using Android.Widget;
using Plugin.Media;
using System;
using System.IO;
using Plugin.CurrentActivity;
using Android.Graphics;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace AppAccesoAzureStorage
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        string Archivo;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            
            SetContentView(Resource.Layout.activity_main);
            SupportActionBar.Hide();
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            var Imagen = FindViewById<ImageView>(Resource.Id.imagen);
            var btnGuardar = FindViewById<Button>(Resource.Id.btnguardar);
            var txtNombre = FindViewById<EditText>(Resource.Id.txtnombre);
            var txtIngredientes = FindViewById<EditText>(Resource.Id.txtingredientes);
            var txtTipo = FindViewById<EditText>(Resource.Id.txttipo);
            var txtPrecio = FindViewById<EditText>(Resource.Id.txtprecio);
            Imagen.Click += async delegate
            {
                await CrossMedia.Current.Initialize();
                var archivo = await CrossMedia.Current.TakePhotoAsync
                (new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    Directory = "Imagenes",
                    Name = txtNombre.Text,
                    SaveToAlbum = true,
                    CompressionQuality = 30,
                    CustomPhotoSize = 30,
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                    DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front
                });
                if (archivo == null)
                    return;
                Bitmap bp = BitmapFactory.DecodeStream(archivo.GetStream());
                Archivo = System.IO.Path.Combine(System.Environment.GetFolderPath
                    (System.Environment.SpecialFolder.Personal), txtNombre.Text + ".jpg");
                var stream = new FileStream(Archivo, FileMode.Create);
                bp.Compress(Bitmap.CompressFormat.Jpeg, 30, stream);
                stream.Close();
                Imagen.SetImageBitmap(bp);
            };

            btnGuardar.Click += async delegate
            {
                try
                {
                    var CuentadeAlmacenamiento = CloudStorageAccount.Parse
                    //cadena mia
                    //("DefaultEndpointsProtocol=https;AccountName=datosdeclasepablo;AccountKey=+FTcUo1hqSQbfyFCHi991/14uV4yVx8PuQEJPgKZ3aWLMWIFczojIWcr0tNnT6mNvN/rDEJL1Iluao2s1/G/kA==;EndpointSuffix=core.windows.net");
                    ("DefaultEndpointsProtocol=https;AccountName=datosdeclasechris;AccountKey=vZSdTw3yWhlh/aF4mzz+XTUg9TbTQXjOtGFz5q1s0mLyHqo5o5/TxcZ6tGbiH+pOVMAXODwpIqV0H0YW7RlL0A==;EndpointSuffix=core.windows.net");
                    //cadena del profe
                    //("DefaultEndpointsProtocol=https;AccountName=datosdeclase;AccountKey=pw60bkc2JlQlTQ8GtD+3PaYGYbdR2F1YmJqGCqfs4LUmYGth6VcFZhm5L0pwsJvazEg/aULu9vWOl9mUeCdOmw==;EndpointSuffix=core.windows.net");
                    //pa subir foto tomada
                    var ClienteBlob = CuentadeAlmacenamiento.CreateCloudBlobClient();
                    var Carpeta = ClienteBlob.GetContainerReference("imagenesmenu");
                    var resourceBlob = Carpeta.GetBlockBlobReference(txtNombre.Text + ".jpg");
                    resourceBlob.Properties.ContentType = "image/jpeg";
                    await resourceBlob.UploadFromFileAsync(Archivo);
                    Toast.MakeText(this, "imagen Almacenada en contenedor de Azure",
                    ToastLength.Long).Show();

                    //pa subir datos del textbox
                    var TablaNoSQL = CuentadeAlmacenamiento.CreateCloudTableClient();
                    var Coleccion = TablaNoSQL.GetTableReference("menu");
                    await Coleccion.CreateIfNotExistsAsync();
                    var producto = new Menu("Menu", txtNombre.Text);
                    producto.Ingredientes = txtIngredientes.Text;
                    producto.Tipo = txtTipo.Text;
                    producto.Precio = double.Parse(txtPrecio.Text);
                    producto.Imagen = "https://datosdeclasechris.blob.core.windows.net/imagenesmenu/" + txtNombre.Text + ".jpg";
                    var Store = TableOperation.Insert(producto);
                    await Coleccion.ExecuteAsync(Store);
                    Toast.MakeText(this, "Datos Almacenados en tabla NoSQL",
                        ToastLength.Long).Show(); ;
                }
                catch (Exception ex)
                {
                    Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                }
            };
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    public class Menu:TableEntity
    {
        public Menu(string Categoria, string Nombre)
        {
            PartitionKey = Categoria;
            RowKey = Nombre;
        }
        public string Ingredientes { get; set; }
        public string Tipo { get; set; }
        public double Precio { get; set; }
        public string Imagen { get; set; }

    }
}